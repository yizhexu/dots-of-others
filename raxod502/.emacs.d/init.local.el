;; code that should be run at the very beginning of init, e.g.


(radian-local-on-hook before-straight

  ;; code that should be run right before straight.el is bootstrapped,
  ;; e.g.
  )

(radian-local-on-hook after-init

  ;; code that should be run at the end of init, e.g.

  ;;; `exec-path-from-shell' provides a way to load your exec path to emacs
  ;; Based on https://github.com/purcell/exec-path-from-shell/issues/75
  ;;(use-package exec-path-from-shell
  ;;  :init
  ;;  (add-hook 'after-init-hook 'exec-path-from-shell-initialize))

  ;; Note that shell-command-to-string captures both stdout and stderr. Luckily we can just pipe the stderr to /dev/null: 
  ;; linux/osx using -n to stop ouputing the trailing newline
  (setq my_shell_output (shell-command-to-string "/bin/echo -n $PATH 2>/dev/null"))
  (symbol-value 'my_shell_output)
  (setenv "PATH" my_shell_output)
  (add-to-list 'exec-path 'my_shell_output)
  
  ;;; gorepl-mode - A minor emacs mode for Go REPL.
  ;;; https://github.com/manute/gorepl-mode
  (use-package gorepl-mode
    :defer t
    :commands (gorepl-run)
    ;; default setup mapping (this will override `go-goto-map')
    :hook (go-mode-hook . gorepl-mode))
 )

;; see M-x customize-group RET radian-hooks RET for which hooks you
;; can use with `radian-local-on-hook'
