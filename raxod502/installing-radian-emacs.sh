#!/bin/bash

# Depends on:
# - yay
# - golang
# - emacs

#+ Source [[https://gitlab.com/toms-dotfiles/utilities/-/blob/master/safe_linker.sh][safe_linker]]
safe_linker_url='https://gitlab.com/toms-dotfiles/utilities/-/raw/master/safe_linker.sh'
#-NOTE: POSIX compliant version of 'source <(curl -s "${safe_linker_url}")'
tmppipe=$(mktemp -u)
mkfifo -m 600 "${tmppipe}"
curl -s "${safe_linker_url}" >"${tmppipe}" &
. "${tmppipe}"

#-TODO: interactively ask which languages should be configured
# bash
  bash=(bash-language-server)
# C/C++
  # c=(clang)
# Flow
  # flow=(flow-bin)
# Golang - add "${GOPATH}"/bin to your "${PATH}"
  config_golang () {
      GO111MODULE=on go get golang.org/x/tools/gopls@latest
      go get github.com/motemen/gore/cmd/gore
  }
# Haskell
  # haskell=(haskell-ide-engine)
# HTML
  html=(vscode-html-languageserver-bin)
# JavaScript/TypeScript
  # javascript=(prettier typescript-language-server-bin)
# Python - the language server is downloaded automatically courtesy of lsp-python-ms.
  python=(python-black)
# LaTeX
  latex=(digestif)

#-TODO: figure out why `--needed` isint working for AUR packages while
# using yay and fix it (https://github.com/Jguer/yay/issues/46)
yay -S --needed "${bash[@]}" "${html[@]}" "${python[@]}" "${latex[@]}" # "${flow[@]}" "${c[@]}" "${haskell[@]}" "${javascript[@]}"
config_golang

mkdir -p "${HOME}"/.emacs.d/straight/versions
mkdir -p "${HOME}"/dotfiles/dots-of-others/raxod502
cd "${HOME}"/dotfiles/dots-of-others/raxod502 || exit 1

# Clone as a submodule and init, if exists update and init if needed
if [ -e ./radian ] ; then
    git submodule update --init radian
else
    git submodule add -b develop -f https://github.com/raxod502/radian.git ./radian && git submodule update --init radian
fi

safe_linker "$(pwd)"/radian/emacs/early-init.el "$(pwd)"/.emacs.d/early-init.el
safe_linker "$(pwd)"/radian/emacs/init.el "$(pwd)"/.emacs.d/init.el
safe_linker "$(pwd)"/radian/emacs/versions.el "$(pwd)"/.emacs.d/straight/versions/radian.el

safe_linker "$(pwd)"/.emacs.d "${HOME}"/.emacs.d
